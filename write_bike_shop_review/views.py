from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, Paginator
from django.http import QueryDict
from django.shortcuts import render, redirect
from main.models import BikeShop

from write_bike_shop_review.forms import CreateReviewForm
from write_bike_shop_review.models import BikeShopReview
from main.models import BikeShop

@login_required(login_url='main:login')
def create_review_handler(req, shopid):
    if req.method == 'POST':
        postBody = req.POST.copy()
        postBody['user'] = req.user.pk
        shop = BikeShop.objects.get(id=shopid)
        postBody['shop'] = shop
        reviewForm = CreateReviewForm(data=postBody)
        if reviewForm.is_valid():
            reviewForm.save()
            messages.success(req, "Review has been added successfully")
            return redirect('write_bike_shop_review:read_review', shopid=shopid)
        else:
            messages.error(req, "Review can not be empty")
            return redirect('write_bike_shop_review:read_review', shopid=shopid)
    context = {'shopid': shopid}
    return render(req, 'shops/reviews/add.html', context=context)

@login_required(login_url='main:login')
def delete_review_handler(req, shopid, reviewid):
    toBeDeleted = BikeShopReview.objects.get(id = reviewid)
    if req.user == toBeDeleted.user:
        toBeDeleted.delete()
        messages.success(req, "Review has been deleted successfully")
    else:
        messages.error(req, "You don't have permission to delete this review!")
    return redirect('write_bike_shop_review:read_review', shopid=shopid)

@login_required(login_url='main:login')
def read_review_handler(req, shopid):
    shop = BikeShop.objects.get(id=shopid)
    reviews = BikeShopReview.objects.filter(shop = shop)
    reviewcount = reviews.count()
    context = {'reviews': reviews, 'shop': shop, 'shopid': shopid, 'reviewcount': reviewcount}
    return render(req, "shops/reviews/list.html", context=context) # Render to bike shop review

@login_required(login_url='main:login')
def update_review_handler(req, shopid, reviewid):
    review = BikeShopReview.objects.get(id = reviewid)
    if req.user == review.user:
        if req.method == 'POST':
            postBody = req.POST.copy()
            postBody['user'] = req.user.pk
            shop = BikeShop.objects.get(id=shopid)
            postBody['shop'] = shop
            reviewForm = CreateReviewForm(data=postBody)
            if reviewForm.is_valid():
                review.content = req.POST['content']
                review.save()
                messages.success(req, "Review has been updated successfully")
                return redirect('write_bike_shop_review:read_review', shopid=shopid)
            else:
                messages.error(req, "Review can not be empty")
                return redirect('write_bike_shop_review:read_review', shopid=shopid)
        context = {'shopid': shopid, 'review': review}
        return render(req, 'shops/reviews/edit.html', context=context)
    else:
        messages.error(req, "You don't have permission to edit this review!")
    return redirect('write_bike_shop_review:read_review', shopid=shopid)