from django.apps import AppConfig


class WriteBikeShopReviewConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'write_bike_shop_review'
