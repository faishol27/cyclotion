from django import forms

from write_bike_shop_review.models import BikeShopReview

class CreateReviewForm(forms.ModelForm):
    class Meta:
        model = BikeShopReview
        fields = ("shop", "user", "content")