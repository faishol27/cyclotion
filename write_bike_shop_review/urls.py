from django.urls import path
import write_bike_shop_review.views as views

app_name = 'write_bike_shop_review'

urlpatterns = [
    path('shop/<int:shopid>/reviews/', views.read_review_handler, name='read_review'),
    path('shop/<int:shopid>/reviews/add', views.create_review_handler, name='create_review'),
    path('shop/<int:shopid>/review/<int:reviewid>/edit', views.update_review_handler, name='update_review'),
    path('shop/<int:shopid>/review/<int:reviewid>/delete', views.delete_review_handler, name='delete_review'),
]