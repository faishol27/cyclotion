from django.db import models

from django.contrib.auth.models import User
from main.models import BikeShop

class BikeShopReview(models.Model):
    content = models.TextField(null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shop = models.ForeignKey(BikeShop, on_delete=models.CASCADE)