"""cyclotion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import index_handler

urlpatterns = [
    path('admin/', admin.site.urls),
    path('routes/', include('routecycling.urls', namespace='route')),
    path('', include('shopproduct.urls', namespace='product')),
    path('articles/', include('article.urls', namespace='article')),
    path('', include('main.urls', namespace='main')),
    path('', include('dummy.urls', namespace='dummy')),
    path('', index_handler, name='index'),
    path('', include('write_bike_shop_review.urls', namespace='write_bike_shop_review')),
]
