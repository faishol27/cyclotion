from django.shortcuts import render, redirect

def index_handler(req):
    return render(req, 'index.html')