from django.urls import path
import shopproduct.views as views

from django.conf.urls.static import static
from django.conf import settings

app_name = 'product'

urlpatterns = [
    path('shops/', views.shoplist_handler, name='shop-list'),
    path('shops/search/', views.shoplistsearch_handler, name='shop-search'),

    path('shops/me', views.productlist_private_handler, name='product-private'),
    path('shop/<int:shopid>/products', views.productlist_public_handler, name='product-public'),

    path('products/add', views.productadd_handler, name='product-add'),
    path('products/<int:productid>/edit', views.productedit_handler, name='product-edit'),
    path('products/<int:productid>/delete', views.productdelete_handler, name='product-delete'),
]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)