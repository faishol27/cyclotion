from django.contrib.auth.models import User
from django.db import models
from main.models import BikeShop


# Create your models here.
class ShopItem(models.Model):
    PRODUCT_TYPE = (
        ('Service', 'Service'),
        ('Product', 'Product'),
    )

    bikeshop = models.ForeignKey(BikeShop, on_delete=models.CASCADE, null=True)
    productType = models.CharField(max_length=100, choices=PRODUCT_TYPE)
    productId = models.AutoField(primary_key=True)
    productName = models.CharField(max_length=100, null=False)
    productDesc = models.TextField(default="")
    productPics = models.ImageField()
    productPrice = models.DecimalField(max_digits=30, decimal_places=2)
    productAvailability = models.BooleanField(default=True)