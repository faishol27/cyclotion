from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.paginator import EmptyPage, Paginator
from django.contrib.auth.decorators import login_required
from shopproduct.models import ShopItem

from shopproduct.forms import AddProductForm, EditProductForm
from main.models import BikeShop
from write_bike_shop_review.models import BikeShopReview

# Create your views here.
def get_paginator(request, obj):
    numPage = request.GET.get('page', '1')
    if not numPage.isdigit():
        numPage = 1
    numPage = int(numPage)
    try:
        objPage = Paginator(obj, 25).page(numPage)
        obj = objPage.object_list
    except EmptyPage:
        obj = []
    return {
        'objects': obj,
        'page': [numPage - 1, numPage, numPage + 1]
    }

@login_required(login_url='main:login')
def productlist_public_handler(req, shopid):
    shop = BikeShop.objects.get(id=shopid)
    reviews = BikeShopReview.objects.filter(shop=shop)
    reviewcount = reviews.count()
    shopItem = ShopItem.objects.filter(bikeshop = shop)
    context = {
        'shopid': shopid,
        'shop': shop,
        'shopitem': shopItem,
        'reviewcount': reviewcount
    }
    return render(req, "shops/products/public.html", context)

@login_required(login_url='main:login')
def productlist_private_handler(req):
    if (req.session["bikeshop"] == False):
        return redirect('main:dashboard')
    obj = ShopItem.objects.filter(bikeshop = BikeShop.objects.get(user_id = req.session["id"]))
    return render(req, "shops/products/private.html", context=get_paginator(req, obj))

@login_required(login_url='main:login')
def productadd_handler(req):
    addProductForm = AddProductForm()
    if req.method == 'POST':
        addProductForm = AddProductForm(data=req.POST.copy())
        if addProductForm.is_valid():
            if (len(req.FILES) != 0):
                filename = req.FILES["productPics"].name
                if (not(filename.endswith('.png') or filename.endswith('.jpg') or filename.endswith('.jpg'))):
                    messages.error(req, "Invalid image format, please upload the appropriate image file")
                    addProductForm = AddProductForm()
                    return redirect('product:product-add')
            addProductForm.save(user_id=req.session["id"], productPics=req.FILES)
            addProductForm = AddProductForm()
            messages.success(req, "Product has been added succesfully")
            return redirect('product:product-private')
    return render(req, "shops/products/add.html")

@login_required(login_url='main:login')
def productedit_handler(req, productid):
    product = ShopItem.objects.get(productId = productid)
    context = {'product':product}
    editProductForm = EditProductForm()
    if req.method == 'POST':
        editProductForm = EditProductForm(data=req.POST.copy())
        if editProductForm.is_valid():
            if (len(req.FILES) != 0):
                filename = req.FILES["productPics"].name
                if (not(filename.endswith('.png') or filename.endswith('.jpg') or filename.endswith('.jpg'))):
                    messages.error(req, "Invalid image format, please upload the appropriate image file")
                    editProductForm = EditProductForm()
                    return redirect('product:product-edit', productid=productid)
            editProductForm.save(product_id=productid, productPics=req.FILES)
            editProductForm = EditProductForm()
            messages.success(req, "Product has been edited succesfully")
            return redirect('product:product-private')
    return render(req, "shops/products/edit.html", context=context)

@login_required(login_url='main:login')
def productdelete_handler(req, productid):
    product = ShopItem.objects.get(productId = productid)
    print(product)
    product.delete()
    messages.success(req, "Product has been deleted succesfully")
    return redirect('product:product-private')

@login_required(login_url='main:login')
def shoplist_handler(req):
    bikeShops = BikeShop.objects.all()
    title = "Shop"
    context = {"bikeShops": bikeShops, "title": title}
    return render(req, "shops/list.html", context=context)

def shoplistsearch_handler(req):
    query = req.GET.get('query', '')
    title = "Search results for \"" + query + "\""
    bikeShops = []
    bikeShops.extend(list(BikeShop.objects.filter(description__icontains=query)))
    bikeShops.extend(list(BikeShop.objects.filter(address__icontains=query)))
    bikeShops.extend(list(BikeShop.objects.filter(user__first_name__icontains=query)))
    bikeShops = list(dict.fromkeys(bikeShops))
    context = {"bikeShops": bikeShops, "title": title}
    return render(req, "shops/list.html", context=context)
