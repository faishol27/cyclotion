from django.contrib import admin

from shopproduct.models import ShopItem

# Register your models here.
admin.site.register(ShopItem)