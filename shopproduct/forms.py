from django import forms
from main.models import BikeShop
from shopproduct.models import ShopItem
    
PRODUCT_TYPE = (
    ('Service', 'Service'),
    ('Product', 'Product'),
)


class AddProductForm(forms.Form):

    productType = forms.ChoiceField(choices=PRODUCT_TYPE)
    productName = forms.CharField()
    productDesc = forms.CharField(widget=forms.Textarea, required=False)
    productPics = forms.ImageField(required=False)
    productPrice = forms.DecimalField(max_digits=30, decimal_places=2)
    productAvailability = forms.BooleanField(initial=False, required=False)

    def save(self, user_id, productPics):
        print(self.cleaned_data)
        productType = self.cleaned_data.get("productType")
        productName = self.cleaned_data.get("productName")
        productDesc = self.cleaned_data.get("productDesc")
        if (len(productPics) != 0):
            productPics = productPics["productPics"]
        else:
            productPics = self.cleaned_data.get("productPics")
        productPrice = self.cleaned_data.get("productPrice")
        productAvailability = self.cleaned_data.get("productAvailability")
        ShopItem.objects.create(
                                bikeshop = BikeShop.objects.get(user_id=user_id),
                                productType = productType,
                                productName = productName,
                                productDesc = productDesc,
                                productPics = productPics,
                                productPrice = productPrice,
                                productAvailability = productAvailability
                                )

class EditProductForm(forms.Form):

    productType = forms.ChoiceField(choices=PRODUCT_TYPE)
    productName = forms.CharField()
    productDesc = forms.CharField(widget=forms.Textarea, required=False)
    productPics = forms.ImageField(required=False)
    productPrice = forms.DecimalField(max_digits=30, decimal_places=2)
    productAvailability = forms.BooleanField(initial=False, required=False)

    def save(self, product_id, productPics):
        print(self.cleaned_data)
        productType = self.cleaned_data.get("productType")
        productName = self.cleaned_data.get("productName")
        productDesc = self.cleaned_data.get("productDesc")
        productPrice = self.cleaned_data.get("productPrice")
        productAvailability = self.cleaned_data.get("productAvailability")
        product = ShopItem.objects.get(productId=product_id)
        product.productType = productType
        product.productName = productName
        product.productDesc = productDesc
        if (len(productPics) != 0):
            productPics = productPics["productPics"]
            product.productPics = productPics
        product.productPrice = productPrice
        product.productAvailability = productAvailability
        product.save()                       