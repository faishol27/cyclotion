from django.db import models
from django.contrib.auth.models import User

class Route(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=False)
    description = models.TextField(default="")
    coordinate_data = models.TextField(null=False)
    
