from django.apps import AppConfig


class RoutecyclingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'routecycling'
