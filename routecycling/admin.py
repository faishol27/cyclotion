from django.contrib import admin

from routecycling.models import Route

class RouteAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user')

admin.site.register(Route, RouteAdmin)