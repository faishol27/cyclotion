from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, Paginator
from django.http import QueryDict
from django.shortcuts import render, redirect

from routecycling.forms import CreateRouteForm
from routecycling.models import Route

def get_paginator(request, obj):
    numPage = request.GET.get('page', '1')
    if not numPage.isdigit():
        numPage = 1
    numPage = int(numPage)
    try:
        objPage = Paginator(obj, 25).page(numPage)
        obj = objPage.object_list
    except EmptyPage:
        obj = []
    return {
        'objects': obj,
        'page': [numPage - 1, numPage, numPage + 1]
    }


@login_required(login_url='main:login')
def routelist_public_handler(req):
    return render(req, "routes/public.html")

@login_required(login_url='main:login')
def routelist_private_handler(req):
    obj = Route.objects.filter(user = req.user)
    return render(req, "routes/private.html", context=get_paginator(req, obj))

@login_required(login_url='main:login')
def routeadd_handler(req):
    routeForm = CreateRouteForm()
    if req.method == 'POST':
        postBody = req.POST.copy()
        postBody['user'] = req.user.pk
        routeForm = CreateRouteForm(data=postBody)
        if routeForm.is_valid():
            routeForm.save()
            messages.success(req, "Your route has been add!")
            return redirect('main:dashboard')
    return render(req, "routes/add.html", {'form': routeForm})

