from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from routecycling.models import Route

class MainTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        route = Route(user = user, name='Route Test', description='Route desc')
        route.save()

    def test_view_route_public(self):
        resp = self.client.get(reverse('route:route-public'))
        self.assertEqual(resp.status_code, 200)
    
    def test_view_route_private(self):
        resp = self.client.get(reverse('route:route-private'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Route Test")
        self.assertContains(resp, "Add")

    def test_view_route_private_emptypage(self):
        resp = self.client.get(reverse('route:route-private'), {'page': 1000})
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Add")

    def test_view_route_private_invalidpage(self):
        resp = self.client.get(reverse('route:route-private'), {'page': 'hahah'})
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Add")

    def test_add_route_get_valid(self):
        resp = self.client.get(reverse('route:route-add'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "input")

    def test_add_route_post_valid(self):
        data = {
            "name": "Haha",
            "description": "Uhuy",
            "coordinate_data": "[]"
        }
        resp = self.client.post(reverse('route:route-add'), data, follow=True)
        RED_CHAIN = [('/dashboard/', 302)]
        self.assertEqual(resp.redirect_chain, RED_CHAIN)

        routes = Route.objects.all()
        self.assertEqual(routes.count(), 2)

    
