from django import forms

from routecycling.models import Route

class CreateRouteForm(forms.ModelForm):
    class Meta:
        model = Route
        fields = ("name", "description", "coordinate_data", "user")
    