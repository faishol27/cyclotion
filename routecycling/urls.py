from django.urls import path
import routecycling.views as views

app_name = 'route'

urlpatterns = [
    path('', views.routelist_public_handler, name='route-public'),
    path('me', views.routelist_private_handler, name='route-private'),
    path('add', views.routeadd_handler, name='route-add'),
]