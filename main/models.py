from django.db import models
from django.contrib.auth.models import User

class Cyclist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class BikeShop(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.TextField(null=False)
    description = models.TextField(null=False)
    verified = models.BooleanField(default=False)
    id_card = models.TextField(null=False)
