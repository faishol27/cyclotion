from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        RED_CHAIN = [('/login/', 302)]
        resp = self.client.get('/logout/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

    def test_login_url_get_noauth(self):
        resp = self.client.get('/login/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/login.html')

    def test_register_url_get_noauth(self):
        resp = self.client.get('/register/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/register.html')

    def test_login_url_post_invalid(self):
        resp = self.client.post('/login/', {'username': 'asdf', 'lol': 'ikimasyo'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/login.html')

    def test_register_url_post_invalid(self):
        resp = self.client.post('/register/', {'username': 'asdf', 'lol': 'ikimasyo'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/register.html')

    def test_register_url_post_password_missmatch(self):
        data = {
            'username': 'test1',
            'name': 'test1',
            'email': 'test1@test.com',
            'role': 'cyclist', 
            'password1': 'Halo12345678',
            'password2': 'sdfdsfdsf'
        }
        resp = self.client.post('/register/', data, follow = True)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'password fields didn&#x27;t match')

    def test_register_url_post_bikeshop_invalid(self):
        data = {
            'username': 'test',
            'name': 'test',
            'email': 'test@test.com',
            'role': 'bikeshop', 
            'password1': 'Halo12345678',
            'password2': 'Halo12345678',
            'address': 'some'
        }
        resp = self.client.post('/register/', data, follow = True)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'fill all the fields')

    def test_login_url_post_valid(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/dashboard/', 302)]
        resp = self.client.post('/login/', {'username': 'test', 'password': 'Halo1234'}, follow = True)
        self.assertEqual(resp.redirect_chain, RED_CHAIN)

    def test_register_url_post_cyclist_valid(self):
        data = {
            'username': 'test',
            'name': 'test',
            'email': 'test@test.com',
            'role': 'cyclist', 
            'password1': 'Halo12345678',
            'password2': 'Halo12345678'
        }
        resp = self.client.post('/register/', data, follow = True)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'created successfully')
    
    def test_register_url_post_bikeshop_valid(self):
        data = {
            'username': 'test',
            'name': 'test',
            'email': 'test@test.com',
            'role': 'bikeshop', 
            'password1': 'Halo12345678',
            'password2': 'Halo12345678',
            'address': 'some',
            'description': 'inideskripsi',
            'id_card': 'ada'
        }
        resp = self.client.post('/register/', data, follow = True)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'created successfully')

    def test_login_url_auth(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/dashboard/', 302)]

        resp = self.client.get('/login/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)
    
    def test_register_url_auth(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/dashboard/', 302)]

        resp = self.client.get('/register/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

