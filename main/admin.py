from django.contrib import admin

from main.models import BikeShop

# Register your models here.
admin.site.register(BikeShop)