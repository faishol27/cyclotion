from django import forms
from django.contrib.auth import decorators
from django.contrib.auth.models import User, Group

from main.models import Cyclist, BikeShop

ROLE_CHOICE = (
    ('cyclist', 'cyclist'),
    ('bikeshop', 'bikeshop')
)

class RegisterForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
    }
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    address = forms.CharField(required=False)
    description = forms.CharField(required=False)
    id_card = forms.CharField(required=False)
    role = forms.ChoiceField(choices=ROLE_CHOICE)

    class Meta:
        model = User
        fields = ("username", "first_name", "email")
    
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2
    
    def clean_role(self):
        role = self.cleaned_data.get("role")
        if role == "bikeshop":
            address = self.cleaned_data.get("address")
            description = self.cleaned_data.get("description")
            id_card = self.cleaned_data.get("id_card")
            if not(address and id_card and description):
                raise forms.ValidationError("You have to fill all the fields.")
        return role

    def save(self, commit=True):
        role = self.cleaned_data.get("role")
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            if role == "cyclist":
                Cyclist.objects.create(user = user)
            else:
                address = self.cleaned_data.get("address")
                description = self.cleaned_data.get("description")
                id_card = self.cleaned_data.get("id_card")
                BikeShop.objects.create(user = user, address = address, id_card = id_card, description=description)
        return user