from django.contrib import messages
from django.contrib.auth import logout, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, Paginator
from django.shortcuts import render, redirect
from main.models import BikeShop

from main.forms import RegisterForm
from routecycling.models import Route
from routecycling.views import get_paginator

def register_handler(req):
    if req.user.is_authenticated: return redirect('main:dashboard')

    registerForm = RegisterForm()
    if req.method == 'POST':
        registerForm = RegisterForm(data=req.POST.copy())
        if registerForm.is_valid():
            registerForm.save()
            messages.success(req, "Your account was created successfully.")
            registerForm = RegisterForm()
    return render(req, 'accounts/register.html', {'form': registerForm})

def login_handler(req):
    if not req.user.is_authenticated:
        authForm = AuthenticationForm()
        if req.method == 'POST':
            authForm = AuthenticationForm(data=req.POST.copy())
            if authForm.is_valid():
                user = authForm.get_user()
                if user != None:
                    login(req, authForm.get_user())
                    return redirect('main:dashboard')
        context = { 'active': 'login', 'form': authForm }
        return render(req, 'accounts/login.html', context = context)
    else:
        return redirect('main:dashboard')

def logout_handler(req):
    logout(req)
    return redirect('main:login')

@login_required(login_url='main:login')
def dashboard(req):
    try:
        print(BikeShop.objects.get(user_id=req.user.id))
        req.session["bikeshop"] = True
    except BikeShop.DoesNotExist:
        req.session["bikeshop"] = False
    req.session["id"] = req.user.id
    obj = Route.objects.filter(user = req.user)
    return render(req, "accounts/dashboard.html", context=get_paginator(req, obj))