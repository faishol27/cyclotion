from django.shortcuts import render, redirect

def shoplist_handler(req):
    return render(req, "shops/list.html")


def articlelist_public_handler(req):
    return render(req, "articles/public.html")

def articlelist_private_handler(req):
    return render(req, "articles/private.html")

def articleadd_handler(req):
    if req.method == 'POST':
        return redirect('dummy:article-private')
    return render(req, "articles/add.html")

def articleedit_handler(req, articleid):
    context = {
        'article': {}
    }
    if req.method == 'POST':
        return redirect('dummy:article-private')
    return render(req, "articles/edit.html", context=context)

def articledelete_handler(req, articleid):
    return redirect('dummy:article-private')

def articleview_handler(req, articleid):
    context = {"notificationIsSuccessful": True}
    comments = commentlist_handler(articleid)
    return render(req, "articles/view.html", context=context)
    
def commentlist_handler(articleid):
    pass

def reviewlist_handler(req, shopid):
    context = {
        'shopid': shopid
    }
    return render(req, "shops/reviews/list.html", context)

def reviewadd_handler(req, shopid):
    context = {
        'shopid': shopid
    }
    if req.method == 'POST':
        return redirect('dummy:review-list', shopid=shopid)
    return render(req, "shops/reviews/add.html", context)

def reviewedit_handler(req, shopid, reviewid):
    if req.method == 'POST':
        return redirect('dummy:review-list', shopid=shopid)
    context = {
        'shopid': shopid
    }
    return render(req, "shops/reviews/edit.html", context)

def reviewdelete_handler(req, shopid, reviewid):
    return redirect('main:dashboard')
    