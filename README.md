# Cyclotion

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]](commits-gl)

CSF3600202 - Rekayasa Perangkat Lunak Semester Ganjil 2020/2021

Fakultas Ilmu Komputer, Universitas Indonesia

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Latar Belakang Aplikasi](#latar-belakang)
- [Anggota Kelompok](#anggota-kelompok)
- [Daftar Fitur](#daftar-fitur) 
- [Link Herokuapp](#link-herokuapp)

## Latar Belakang
Peningkatan pesepeda di Indonesia selama PSBB membuat angka pemula dalam hal bersepeda turut meningkat. Hal ini akan menyebabkan mereka merasa kebingungan dalam mencari tempat membeli sepeda beserta perlengkapannya, merencanakan perjalanan bersepeda, hingga mencari tempat memperbaiki sepeda.

## Anggota Kelompok
1. 1906285522 - Julian Fernando
2. 1906285573 - Muhammad Faishol Amirul Mukminin
3. 1906285560 - Raden Fausta Anugrah Dianparama
4. 1906285592 - Samuel
5. 1906350995 - Efrado Suryadi

## Daftar Fitur
TBW

## Link Herokuapp
Repository ini akan dideploy di [https://cyclotion.herokuapp.com](https://cyclotion.herokuapp.com)

[pipeline-badge]: https://gitlab.com/faishol27/cyclotion/badges/deploy/pipeline.svg
[coverage-badge]: https://gitlab.com/faishol27/cyclotion/badges/deploy/coverage.svg
[commits-gl]: https://gitlab.com/faishol27/cyclotion/-/commits/deploy