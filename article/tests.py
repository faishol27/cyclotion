from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from article.models import Article, VoteArticle

class VoteTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        article = Article(user = user, title='Ehe', content='Ini artikel lho')
        article.save()

    def test_view_article_unvote(self):
        resp = self.client.get(reverse('article:article-view',args=[1]))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Ehe")
        self.assertContains(resp, "-/10")
        self.assertContains(resp, "<strong>0</strong>/10")
        self.assertNotContains(resp, "href=\"" + reverse('article:vote-handler',args=[1]))
    
    def test_add_vote_article_valid(self):
        resp = self.client.post(reverse('article:vote-handler',args=[1]), {'rating': 8}, follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 1)

        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Ehe")
        self.assertContains(resp, "8.0/10")
        self.assertContains(resp, "<strong>8.0</strong>/10")
        self.assertContains(resp, "href=\"" + reverse('article:vote-handler',args=[1]))

    def test_add_vote_article_invalid(self):
        resp = self.client.post(reverse('article:vote-handler',args=[1]), {'rating': 'asdas'}, follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 0)

    def test_add_vote_article_invalid_empty(self):
        resp = self.client.post(reverse('article:vote-handler',args=[1]), {'haha': 'asdas'}, follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 0)

    def test_delete_vote_article(self):
        user = User.objects.get(pk=1)
        article = Article.objects.get(id=1)
        vote = VoteArticle(user=user, article=article, value=9)
        vote.save()

        resp = self.client.get(reverse('article:vote-handler',args=[1]), follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 0)
    
    def test_update_vote_article(self):
        user = User.objects.get(pk=1)
        article = Article.objects.get(id=1)
        vote = VoteArticle(user=user, article=article, value=9)
        vote.save()

        resp = self.client.post(reverse('article:vote-handler',args=[1]), {'rating': 5}, follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 1)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Ehe")
        self.assertContains(resp, "5.0/10")
        self.assertContains(resp, "<strong>5.0</strong>/10")
        self.assertContains(resp, "href=\"" + reverse('article:vote-handler',args=[1]))

    def test_delete_vote_article_otheruser(self):
        user = User.objects.create_user(username = 'baru', password = 'Halo1234')
        article = Article.objects.get(id=1)
        vote = VoteArticle(user=user, article=article, value=9)
        vote.save()

        resp = self.client.get(reverse('article:vote-handler',args=[1]), follow=True)
        votes = VoteArticle.objects.all()
        self.assertEqual(votes.count(), 1)
