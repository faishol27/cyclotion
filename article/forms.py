from django import forms
from django.db.models import fields

from article.models import Article, ArticleComment

class CreateArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ("title", "content", "user")

class CreateArticleCommentForm(forms.ModelForm):
    class Meta:
        model = ArticleComment
        fields = ("comment", "user", "article")
    
