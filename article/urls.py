from django.urls import path
import article.views as views

app_name = 'article'

urlpatterns = [
    path('', views.articlelist_public_handler, name='article-public'),
    path('me', views.articlelist_private_handler, name='article-private'),
    path('add', views.articleadd_handler, name='article-add'),
    path('article/<int:articleid>', views.articleview_handler, name='article-view'),
    path('article/<int:articleid>/edit', views.articleedit_handler, name='article-edit'),
    path('article/<int:articleid>/delete', views.articledelete_handler, name='article-delete'),
    path('article/<int:articleid>/comment/add', views.addcomment_handler, name='article-commentadd'),
    path('article/<int:articleid>/comment/<int:commentid>/delete', views.deletecomment_handler, name='article-commentdelete'),
    path('article/<int:articleid>/vote', views.vote_handler, name='vote-handler'),
]
