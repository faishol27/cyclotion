from django.contrib import admin

from article.models import Article, ArticleComment, VoteArticle

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'user')

admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleComment)
admin.site.register(VoteArticle)