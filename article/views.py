from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, Paginator
from django.http import QueryDict
from django.shortcuts import render, redirect

from article.forms import CreateArticleCommentForm, CreateArticleForm
from article.models import Article, ArticleComment, VoteArticle

def get_paginator(request, obj):
    numPage = request.GET.get('page', '1')
    if not numPage.isdigit():
        numPage = 1
    numPage = int(numPage)
    try:
        objPage = Paginator(obj, 25).page(numPage)
        obj = objPage.object_list
    except EmptyPage:
        obj = []
    return {
        'objects': obj,
        'page': [numPage - 1, numPage, numPage + 1]
    }


@login_required(login_url='main:login')
def articlelist_public_handler(req):
    obj = Article.objects.all()
    return render(req, "articles/public.html", context=get_paginator(req, obj))

@login_required(login_url='main:login')
def articlelist_private_handler(req):
    obj = Article.objects.filter(user = req.user)
    return render(req, "articles/private.html", context=get_paginator(req, obj))

@login_required(login_url='main:login')
def articleadd_handler(req):
    articleForm = CreateArticleForm()
    if req.method == 'POST':
        postBody = req.POST.copy()
        postBody['user'] = req.user.pk
        articleForm = CreateArticleForm(data=postBody)
        if articleForm.is_valid():
            articleForm.save()
            messages.success(req, "Article has been added successfully")
            return redirect('article:article-private')
    return render(req, "articles/add.html", {'form': articleForm})

@login_required(login_url='main:login')
def articleedit_handler(req, articleid):
    article = Article.objects.get(id = articleid)
    if article.user.pk != req.user.pk:
        messages.error(req, "You do not have permission to edit this article!")
        return redirect('article:article-private')
    articleForm = CreateArticleForm()
    context = {'article': article, 'form': articleForm}
    if req.method == 'POST':
        postBody = req.POST.copy()
        postBody['user'] = req.user.pk
        articleForm = CreateArticleForm(data=postBody)
        if articleForm.is_valid():
            article.title = postBody['title']
            article.content = postBody['content']
            article.save()
            messages.success(req, "Article has been updated successfully")
            return redirect('article:article-private')
    return render(req, "articles/edit.html",context=context)

@login_required(login_url='main:login')
def articledelete_handler(req, articleid):
    article = Article.objects.get(id = articleid)
    if req.user == article.user:
        article.delete()
        messages.success(req, "Article has been deleted sucessfully")
    else:
        messages.error(req, "You do not have permission to delete this article!")
    return redirect('article:article-private')

@login_required(login_url='main:login')
def articleview_handler(req, articleid):
    article = Article.objects.get(id = articleid)
    comments = ArticleComment.objects.filter(article = article)
    commentsCount = comments.count()
    context = {'article': article, 'comments': comments, 'commentsCount': commentsCount, 'rating': get_vote(article, req.user)}
    return render(req, "articles/view.html", context=context)

@login_required(login_url='main:login')
def addcomment_handler(req, articleid):
    if req.method == 'POST':
        postBody = req.POST.copy()
        postBody['user'] = req.user.pk
        postBody['article'] = Article.objects.get(id=articleid)
        commentForm = CreateArticleCommentForm(data=postBody)
        if commentForm.is_valid():
            commentForm.save()
            messages.success(req, "Comment has been added successfully")
            return redirect('article:article-view', articleid=articleid)

@login_required(login_url='main:login')
def deletecomment_handler(req, articleid, commentid):
    toBeDeleted = ArticleComment.objects.get(id=commentid)
    if req.user == toBeDeleted.user:
        toBeDeleted.delete()
        messages.success(req, "Comment has been deleted successfully")
    else:
        messages.error(req, "You do not have permission to delete this comment!")
    return redirect('article:article-view', articleid=articleid)

def get_vote(article, user):
    allRating = 0
    userVote = None
    try:        
        votes = VoteArticle.objects.filter(article=article)
        for vote in votes:
            allRating += vote.value
        allRating /= votes.count()

        userVote = VoteArticle.objects.get(article=article, user=user)
        userVote = userVote.value
    except:
        pass

    return {
        'all': allRating,
        'user': userVote
    }

@login_required(login_url='main:login')
def vote_handler(req, articleid):    
    try:
        if req.method == 'POST':
            if req.POST.get("rating") == None:
                raise ValueError
            rating = max(int(req.POST.get("rating")), 1)
 
        article = Article.objects.get(id=articleid)
        vote = VoteArticle.objects.get(article=article, user=req.user)
        if req.method == 'POST':
            vote.value = rating
            vote.save()
        else:
            vote.delete()
    except ValueError:
        pass
    except:
        if article != None and req.method == 'POST':
            vote = VoteArticle(article=article, user=req.user, value=rating)
            vote.save()
    return redirect('article:article-view', articleid=articleid)
